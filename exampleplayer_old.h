#ifndef __EXAMPLEPLAYER_OLD_H__
#define __EXAMPLEPLAYER_OLD_H__

#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <limits>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer_Old {

private:
    Side thisside;
    Board *gameboard;
    int boardValue(Board *testboard, Side side);
    int weights[64];

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
