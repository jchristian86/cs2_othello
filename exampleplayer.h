#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <limits>
#include <list>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
    Side thisside;
    Board *gameboard;
    list<int> *moves;
    list<vector<int> > *openingBook;
    // Used to make sure the Opening Book is switched to the correct
    //  orientation only once.
    bool anyMovesYet;
    // Used to skip opening book after it is exhausted.
    bool bookStillGood;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    
    int boardValue(Board *testboard, Side side);
    int checkOpeningBook();
    void rotateOpeningBook();
    void flipOpeningBook();
};

#endif
