I did this assignment solo.

To get the AI tournament-worthy, I went through three iterations:
1. A fairly complicated algorithm that is (probably) similar to what
    DeepKwok uses.
    Essentially, this algorithm maintained a search tree between each move,
    calculated as far ahead as reasonable in the time limit, and used
    a transposition table to reduce redundancy.
    An opening book and (possibly) alpha-beta pruning would be added later.
    Unfortunately, this algorithm ran into a number of issues, particularly
    memory management problems and an inability to select reasonable moves.
    Due to time constraints, it was then scrapped, with some ideas
    applied elsewhere.

2. A recursive minimax function that took in the number of moves desired,
    plus an opening book (take from www.samsoft.org.uk/reversi/openings.htm).
    This algorithm executed correctly (aside from an issue after adding an
    opening book where the internal Board object became incorrect, which
    was fixed by creating a new board out of the move history before running
    the minimax algorithm).
    However, this algorithm had terrible performance, even against the random
    player (SimplePlayer). After a number of changes to the board valuation
    function, I concluded that the problem was in the recursive minimax
    function, even though I couldn't find it. Thus, this algorithm too was
    scrapped, though much of it (especially the opening book) was retained.

3. A hard-coded two move (my move, plus opponent's response) minimax algorithm
    that used the opening book from #2.
    This algorithm is reasonably effective. It consistently beats SimplePlayer
    and can do well and win against both ConstantTimePlayer and BetterPlayer
    (although the latter two win fairly often, as well).
    In this pass, the board valuation function was altered to include the
    number of frontier squares (the number of empty squares next to my tiles),
    which led to a further increase in performance.


Algorithm Analysis

This is a reasonably effective algorithm. The opening book puts the player
in a good position early on, and the valuation function seems to be good enough
to maintain this advantage over similarly complex algorithms. Algorithms that
look far enough into the future will be able to beat this one, and algorithms
with a sufficiently complex opening book will negate the primary advantage of
this algorithm.
