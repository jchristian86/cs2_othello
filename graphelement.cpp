#include "graphelement.h"


using namespace std;

// Standard constructor, nothing particularly special
GraphElement::GraphElement(vector<bool> *thisboard, Side nextplayer,
                map<vector<bool>, GraphElement*> *extantElements){
    this->extantElements = extantElements;
    this->boardstruct = *thisboard;
    this->nextplayer = nextplayer;
    this->parents = list<GraphElement*>();
    this->children = list<GraphElement*>();
}

// Destructor: removes all references to this GraphElement
GraphElement::~GraphElement(){
    GraphElement *temp;
    list<GraphElement*>::iterator it;
    // Remove this GraphElement from its parents
    for(it = this->parents.begin(); it != this->parents.end(); it++){
        temp = *it;
        temp->removeChild(this);
    }
    // Now remove the GraphElement from its children
    for(it = this->children.begin(); it != this->children.end(); it++){
        temp = *it;
        temp->removeParent(this);
        // If a child now has no parents, delete it.
        if(temp->countParents() == 0){
            delete temp;
        }
    }
    // Now, remove the corresponding entry in extantElements
    extantElements->erase(this->boardstruct);
    //GraphElement *temp1 = (*extantElements)[this->boardstruct];
    // Delete the board structure vector
    delete &(this->boardstruct);
}

void GraphElement::addParent(GraphElement *parent){
    this->parents.push_back(parent);
    return;
}

void GraphElement::removeParent(GraphElement *parent){
    this->parents.remove(parent);
    return;
}

int GraphElement::countParents(){
    return this->parents.size();
}

void GraphElement::addChild(GraphElement *child){
    this->children.push_back(child);
    return;
}

void GraphElement::removeChild(GraphElement *child){
    this->children.remove(child);
    return;
}

int GraphElement::countChildren(){
    return this->children.size();
}


list<GraphElement*>* GraphElement::getChildren(){
    return &(this->children);
}

vector<bool>* GraphElement::getBoard(){
    return &(this->boardstruct);
}

/* makeChildren : given the current state of the board and the Side that
    gets the next move:
    - Calculate each possible next board state
    - Check to see if memoized
        * If not, create a new GraphElement for it and memoize
    - Add each GraphElement to the children list
*/
/* For vector<bool> boardstruct :
    boardstruct[2*i] is 1 iff square i is occupied
    boardstruct[2*i+1] is 1 if occupied square i is black,
                          0 if occupied square i is white
*/
void GraphElement::makeChildren(){
    // Make sure the children list is empty.
    children = list<GraphElement*>();
    
    // Variable declarations
    bool isValidMove;
    int X, Y, x, y, dx, dy;
    bool mycolor = 0;
    vector<bool> *ptrnextboard;
    vector<bool> nextboard;
    GraphElement *childElement;
    Side other = (nextplayer == BLACK) ? WHITE : BLACK;
    
    if(nextplayer == BLACK){
        mycolor = 1;
    }
    // Loop through all squares on the board
    for(int pos=0; pos<64; pos++){
        // Check to see if that square is already taken
        if(boardstruct[2*pos] == 1){
            continue;
        }
        X = pos % 8;
        Y = pos / 8;
        // If the square is open, check each direction to see if there is a
        // valid move
        // (essentially copied from the board class)
        isValidMove = 0;
        // Loop through all directions
        for(dx=-1; dx<=1; dx++){
            for(dy=-1; dy<=1; dy++){
                // (skip no direction)
                if(dy == 0 && dx == 0) continue;
                // Look one square in that direction
                x = X + dx;
                y = Y + dy;
                // If that square is on the board and occupied by
                // the other color
                if((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                    (boardstruct[2*(8*y+x)] == 1) &&
                    (boardstruct[2*(8*y+x)+1] != mycolor)){
                    // Keep moving in that direction until pieces of the other
                    // color are exhausted
                    do{
                        x += dx;
                        y += dy;
                    } while ((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                             (boardstruct[2*(8*y+x)] == 1) &&
                             (boardstruct[2*(8*y+x)+1] != mycolor));
                    // If we've reached a piece of our color,
                    // Indicate that a valid move has been found
                    if((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                       (boardstruct[2*(8*y+x)] == 1) &&
                       (boardstruct[2*(8*y+x)+1] == mycolor)){
                        isValidMove = 1;
                    }
                }
                // Break loops as soon as we know the move is valid.
                if(isValidMove) break;
            }
            if(isValidMove) break;
        }
        // If this is not a valid move, continue
        if(not isValidMove) continue;
        
        /* Now we know that pos represents a valid move
            We need to:
                - Create a new board representing the result of that move
                - Check to see if we already have a GraphElement for the board
                    - If not, we create and memoize a new GraphElement
                    - If so, delete the new board to free memory
                - Add this GraphElement to the new one's parents
                - Add the new GraphElement to this one's children
        */
        
        // Copy the current board
        ptrnextboard = new vector<bool>(boardstruct);
        nextboard = *ptrnextboard;
        // use the doMove algorithm in the board class to execute the move
        // (which is already known to be valid)
        // Sample each direction
        for(dx=-1; dx<=1; dx++){
            for(dy=-1; dy<=1; dy++){
                if(dy == 0 && dx == 0) continue;
                x = X;
                y = Y;
                // Go out over all tiles of the other color to see if this
                // direction will have any tiles flip.
                do{
                    x += dx;
                    y += dy;
                } while ((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                         (nextboard[2*(8*y+x)] == 1) &&
                         (nextboard[2*(8*y+x)+1] != mycolor));
                // If tiles should flip in this direction ...
                if((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                   (nextboard[2*(8*y+x)] == 1) &&
                   (nextboard[2*(8*y+x)+1] == mycolor)){
                    x = X + dx;
                    y = Y + dy;
                    // ... pass over it again and flip them
                    while((0 <= x) && (x < 8) && (0 <= y) && (y < 8) &&
                          (nextboard[2*(8*y+x)] == 1) &&
                          (nextboard[2*(8*y+x)+1] != mycolor)){
                        nextboard[2*(8*y+x)+1] = mycolor;
                        x += dx;
                        y += dy;
                    }
                }
            }
        }
        nextboard[2*(8*Y+X)] = 1;
        nextboard[2*(8*Y+X)] = mycolor;
        
        // Now, nextboard has the results of the specific move.
        // Check to see if this result has a graph element already,
        // or if we need to make a new one.
        childElement = (*extantElements)[nextboard];
        if(childElement){
            delete ptrnextboard;
        }
        else{
            childElement = new GraphElement(&nextboard, other, extantElements);
            (*extantElements)[nextboard] = childElement;
        }
        // Put in the necessary connections between this graph element and
        // its child from this move.
        this->addChild(childElement);
        childElement->addParent(this);
    }
    // If no moves are possible, change the color of this GraphElement
    // and set as its own child.
    // (this way, it gets evaluated as the other color in the next round
    //  of calculations).
    if(this->countChildren() == 0){
        this->nextplayer = other;
        this->addChild(this);
    }
    return;
}


// makeScore : calculates the score of the board
/*
    Corners: +3
    Adj to corner: -3
    Other: +1
    Opponent: -1
*/
void GraphElement::makeScore(Side side){
    score = 0;
    bool isCorner, isAdj;
    for(int pos=0; pos<64; pos++){
        isCorner = isAdj = 0;
        // Indicate if the tile in question is a corner
        if((pos == 0) or (pos == 7) or (pos == 56) or (pos == 63)){
            isCorner = 1;
        }
        // Indicate if the tile in question is adjacent to a corner
        if((pos == 1) or (pos == 8) or (pos == 9) or
            (pos == 6) or (pos == 14) or (pos == 15) or
            (pos == 48) or (pos == 49) or (pos == 57) or
            (pos == 54) or (pos == 55) or (pos == 62)){
            isAdj = 1;
        }
        // If the position is occupied ...
        if(boardstruct[2*pos] == 1){
            // If the position is occupied by the desired color ...
            if(((boardstruct[2*pos+1] == 1) && (side == BLACK)) or 
               ((boardstruct[2*pos+1] == 0) && (side == WHITE))){
                // Give the correct Corner, Adj., or other score.
                if(isCorner){
                    score += 3;
                }
                else if(isAdj){
                    score -= 3;
                }
                else score++;
            }
            // If occupied by the opponent, give the opponent score.
            else score--;
        }
    }
    return;
}

int GraphElement::getScore(){
    return score;
}

void GraphElement::setScore(int score){
    this->score = score;
    return;
}

Side GraphElement::getSide(){
    return nextplayer;
}

// bubbleUp : bubbles the score of a board upwards
// Alternately maximize or minimize, depending on whether the side of the
// player of a parent matches the side that is passed up.
void GraphElement::bubbleUp(Side side){
    list<GraphElement*>::iterator it;
    GraphElement *current;
    Side parentSide;
    for(it = parents.begin(); it != parents.end(); it++){
        current = *it;
        parentSide = current->getSide();
        // If the parent is the same side as the one passed up, maximize.
        if((parentSide == side) and (current->getScore() < score)){
            current->setScore(score);
            current->bubbleUp(side);
            continue;
        }
        // Otherwise, minimize.
        if((parentSide != side) and (current->getScore() > score)){
            current->setScore(score);
            current->bubbleUp(side);
            continue;
        }
    }
    return;
}

// resetScores : resets all scores
// Should be called on the top (or current) board before bubbling up
// from branches.
// The Side is necessary to make sure that bubbling up works properly.
// Scores start at minimum for our boards and maximum for opponent's.
// Also resets the score for all children.
void GraphElement::resetScores(Side side){
    if(nextplayer == side){
        score = numeric_limits<int>::min();
    }
    else{
        score = numeric_limits<int>::max();
    }
    list<GraphElement*>::iterator it;
    for(it = children.begin(); it != children.end(); it++){
        (*it)->resetScores(side);
    }
    return;
}
