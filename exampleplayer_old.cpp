#include "exampleplayer_old.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer_Old::ExamplePlayer_Old(Side side) {
    /*  Initialization of the side and a board to store data.
        We only get 30 seconds here.
    */
    thisside = side;
    Board *aboard = new Board();
    this->gameboard = aboard;
    
    /* Board weights:
        * +3 (corners)
        * -3 (adjacent to corners)
        * +1 (everywhere else)
    */
    for(int i=0; i<64; i++){
        weights[i] = 1;
    }
    weights[0] = weights[7] = weights[56] = weights[63] = 3;
    weights[1] = weights[8] = weights[6] = weights[15] = -3;
    weights[48] = weights[57] = weights[55] = weights[62] = -3;
    weights[9] = weights[14] = weights[49] = weights[54] = -3;
    
    // OLD WEIGHTS (left to make it easier to change weights later):
    /* Board weights (top left corner) (symmetric):
     *  99  -8   8   6
     *     -24  -4  -3
     *           7   4
     *               0
     */
    /*
    // Corners: weight = 99
    weights[0] = weights[7] = weights[56] = weights[63] = 99;
    // weight = -8
    weights[1] = weights[8] = weights[6] = weights[15] = -8;
    weights[48] = weights[57] = weights[55] = weights[62] = -8;
    // weight = +8
    weights[2] = weights[16] = weights[5] = weights[23] = 8;
    weights[40] = weights[58] = weights[47] = weights[61] = 8;
    // weight = +6
    weights[3] = weights[24] = weights[4] = weights[31] = 6;
    weights[32] = weights[59] = weights[60] = weights[39] = 6;
    // weight = -24
    weights[9] = weights[14] = weights[49] = weights[54] = -24;
    // weight = -4
    weights[10] = weights[17] = weights[13] = weights[22] = -4;
    weights[41] = weights[50] = weights[46] = weights[53] = -4;
    // weight = -3
    weights[11] = weights[25] = weights[12] = weights[30] = -3;
    weights[33] = weights[51] = weights[38] = weights[52] = -3;
    // weight = 7
    weights[18] = weights[21] = weights[42] = weights[45] = 7;
    // weight = 4
    weights[19] = weights[26] = weights[20] = weights[29] = 4;
    weights[34] = weights[43] = weights[37] = weights[44] = 4;
    */
}

/*
 * Destructor for the player.
 */
ExamplePlayer_Old::~ExamplePlayer_Old() {
    delete this->gameboard;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer_Old::doMove(Move *opponentsMove, int msLeft) {
    // Process opponent's move
    if(thisside == BLACK){
        gameboard->doMove(opponentsMove, WHITE);
    }
    else{
        gameboard->doMove(opponentsMove, BLACK);
    }
    
    // If there are no moves, return -1, -1
    Move *move = new Move(-1, -1);
    if(!(gameboard->hasMoves(thisside))){
        return move;
    }
    
    // HEURISTIC USED:
    /* For each point on the board:
        - Check if it is a valid move
        - If so, make the move
        - Create each possible response to each move.
        - Compute boad value after response
            * 3 points for holding corner
            * -3 points for holding adjacent to corner
            * 1 point for every other piece
            * -1 point for each opponent piece
        - Pick and return the move with the highest minimum value after
            a response.
    */

    // These are used for the first level (calculating this player's move)
    Board *workingboard;
    Move *workingmove = new Move(-1, -1);
    int maxvalue = std::numeric_limits<int>::min();
    int workingval;
    // These are used for the 2nd level (calculating the opponent's
    // possible moves.
    Board *wboardinner;
    Move *wmoveinner = new Move(-1, -1);
    int wvalinner;
    Side otherside = BLACK;
    if(thisside == BLACK){ otherside = WHITE; }
    
    for(int i = 0; i < 64; i++){
        workingmove->setX(i % 8);
        workingmove->setY(i / 8);
        // If this is a valid move
        if(gameboard->checkMove(workingmove, thisside)){
            // Copy the board and make the move
            workingboard = gameboard->copy();
            workingboard->doMove(workingmove, thisside);
            /*
             * Here, we test each possible move the other side makes
             * and do a basic minimax calculation.
             * (Basically copy the code of the outer for loop here)
             */
            workingval = std::numeric_limits<int>::max();
            for(int j = 0; j < 64; j++){
                wmoveinner->setX(j % 8);
                wmoveinner->setY(j / 8);
                if(workingboard->checkMove(wmoveinner, otherside)){
                    wboardinner = workingboard->copy();
                    wboardinner->doMove(wmoveinner, otherside);
                    wvalinner = boardValue(wboardinner, thisside);
                    if(wvalinner < workingval){
                        workingval = wvalinner;
                    }
                    delete wboardinner;
                }
            }
            // If this is a better value than we already have,
            if(workingval > maxvalue){
                // Store this move as the current best.
                move->setX(workingmove->getX());
                move->setY(workingmove->getY());
                maxvalue = workingval;
            }
            // Free the memory used to copy the board.
            delete workingboard;
        }
    }
    // Free the memory used to store the moves.
    delete workingmove;
    delete wmoveinner;
    
    // RANDOM HEURISTIC
    /*  Pick a point at random, and check to see if it is a 
        valid move. Repeat until a valid move is found, then return it.
    */
    /*
    int testx, testy;
    while(true){
        testx = rand() % 8;
        testy = rand() % 8;
        move->setX(testx);
        move->setY(testy);
        if(gameboard->checkMove(move, thisside)){
            break;
        }
    }
    */
    gameboard->doMove(move, thisside);
    return move;
}

int ExamplePlayer_Old::boardValue(Board *testboard, Side side){
    // valaccum is an accumulated board value
    // tempval is the value at each piece, that is added to valaccum
    int valaccum = 0, tempval;
    for(int j=0; j<64; j++){
        if(not testboard->isOccupied(j)){ break; }
        Side whichpiece = testboard->identify(j);
        // Each board position has a definite weight.
        tempval = weights[j];
        // Add the weight of a piece of the right color.
        if(whichpiece == side){
            valaccum += tempval;
        }
        // Subtract 1 for a piece of the wrong color.
        else{
            valaccum -= 1;
        }
    }
    return valaccum;
}
