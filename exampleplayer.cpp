#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /*  Initialization of the side and a board to store data.
        We only get 30 seconds here.
    */
    thisside = side;
    Board *aboard = new Board();
    this->gameboard = aboard;
    
    moves = new list<int>();
    openingBook = new list<vector<int> >();
    // 13 different opening book sequences:
    // (taken from www.samsoft.org.uk/reversi/openings.htm)
    // Heath-Bat 
    vector<int> *heath_bat = new vector<int>;
    heath_bat->push_back(26);
    heath_bat->push_back(18);
    heath_bat->push_back(19);
    heath_bat->push_back(34);
    heath_bat->push_back(25);
    heath_bat->push_back(11);
    heath_bat->push_back(43);
    heath_bat->push_back(17);
    openingBook->push_back(*heath_bat);
    // Iwasaki
    vector<int> *iwasaki = new vector<int>();
    iwasaki->push_back(26);
    iwasaki->push_back(18);
    iwasaki->push_back(19);
    iwasaki->push_back(34);
    iwasaki->push_back(25);
    iwasaki->push_back(11);
    iwasaki->push_back(12);
    iwasaki->push_back(17);
    openingBook->push_back(*iwasaki);
    // Heath-Chimney
    vector<int> *heath_chimney = new vector<int>();
    heath_chimney->push_back(26);
    heath_chimney->push_back(18);
    heath_chimney->push_back(19);
    heath_chimney->push_back(34);
    heath_chimney->push_back(25);
    heath_chimney->push_back(20);
    heath_chimney->push_back(12);
    openingBook->push_back(*heath_chimney);
    // Chimney
    vector<int> *chimney = new vector<int>();
    chimney->push_back(26);
    chimney->push_back(18);
    chimney->push_back(19);
    chimney->push_back(34);
    chimney->push_back(43);
    chimney->push_back(20);
    openingBook->push_back(*chimney);
    // Cow Bat/Bat/Cambridge
    vector<int> *cambridge = new vector<int>();
    cambridge->push_back(26);
    cambridge->push_back(18);
    cambridge->push_back(19);
    cambridge->push_back(34);
    cambridge->push_back(43);
    cambridge->push_back(29);
    cambridge->push_back(25);
    cambridge->push_back(41);
    openingBook->push_back(*cambridge);
    // Aircraft/Feldbord
    vector<int> *aircraft = new vector<int>();
    aircraft->push_back(26);
    aircraft->push_back(18);
    aircraft->push_back(19);
    aircraft->push_back(34);
    aircraft->push_back(43);
    aircraft->push_back(29);
    aircraft->push_back(37);
    aircraft->push_back(11);
    aircraft->push_back(33);
    aircraft->push_back(42);
    openingBook->push_back(*aircraft);
    // Parallel+ (white should avoid this one)
    vector<int> *parallel = new vector<int>();
    parallel->push_back(26);
    parallel->push_back(34);
    parallel->push_back(43);
    openingBook->push_back(*parallel);
    // Swallow
    vector<int> *swallow = new vector<int>();
    swallow->push_back(26);
    swallow->push_back(20);
    swallow->push_back(37);
    swallow->push_back(25);
    swallow->push_back(21);
    swallow->push_back(29);
    openingBook->push_back(*swallow);
    // Bent Ganglion
    vector<int> *ganglion = new vector<int>();
    ganglion->push_back(26);
    ganglion->push_back(20);
    ganglion->push_back(45);
    ganglion->push_back(25);
    ganglion->push_back(21);
    openingBook->push_back(*ganglion);
    // Aubrey/Tanaka
    vector<int> *tanaka = new vector<int>();
    tanaka->push_back(26);
    tanaka->push_back(20);
    tanaka->push_back(45);
    tanaka->push_back(44);
    tanaka->push_back(37);
    tanaka->push_back(46);
    tanaka->push_back(21);
    openingBook->push_back(*tanaka);
    // Tamenori
    vector<int> *tamenori = new vector<int>();
    tamenori->push_back(26);
    tamenori->push_back(20);
    tamenori->push_back(45);
    tamenori->push_back(44);
    tamenori->push_back(37);
    tamenori->push_back(34);
    tamenori->push_back(29);
    tamenori->push_back(46);
    tamenori->push_back(53);
    tamenori->push_back(19);
    tamenori->push_back(21);
    openingBook->push_back(*tamenori);
    // Rose-Bill+
    vector<int> *rose_bill = new vector<int>();
    rose_bill->push_back(26);
    rose_bill->push_back(20);
    rose_bill->push_back(45);
    rose_bill->push_back(44);
    rose_bill->push_back(37);
    rose_bill->push_back(34);
    rose_bill->push_back(29);
    rose_bill->push_back(46);
    rose_bill->push_back(53);
    rose_bill->push_back(43);
    rose_bill->push_back(52);
    openingBook->push_back(*rose_bill);
    // Central Rose-Bill/Dead Draw
    vector<int> *central = new vector<int>();
    central->push_back(26);
    central->push_back(20);
    central->push_back(45);
    central->push_back(44);
    central->push_back(37);
    central->push_back(34);
    central->push_back(29);
    central->push_back(46);
    central->push_back(53);
    central->push_back(38);
    central->push_back(43);
    openingBook->push_back(*central);

    anyMovesYet = 0;
    bookStillGood = 1;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete this->gameboard;
    delete moves;
    delete openingBook;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    Move *move = new Move(-1, -1);
    
    // Process opponent's move
    Side other = (thisside == BLACK) ? WHITE : BLACK;
    gameboard->doMove(opponentsMove, other);
    if((opponentsMove != NULL) and (opponentsMove->getX() != 1)){
        moves->push_back(opponentsMove->getX() + 8*opponentsMove->getY());
    }
    
    // If this is the first move, immediately go to (2, 3)
/*
    if(opponentsMove == NULL){
        anyMovesYet = 1;
        move->setX(2);
        move->setY(3);
        moves->push_back(26);
        gameboard->doMove(move, thisside);
        return move;
    }
*/
    if(thisside == BLACK){
        anyMovesYet = 1;
    }
    // If we're receiving the first move, change Opening Book orientation.
    if(not anyMovesYet){
        int p = opponentsMove->getX() + 8*opponentsMove->getY();
        if((p == 19) or (p == 44)){
            flipOpeningBook();
        }
        if((p == 37) or (p == 44)){
            rotateOpeningBook();
        }
        anyMovesYet = 1;
    }
    // If there are no moves, return -1, -1
    if(!(gameboard->hasMoves(thisside))){
        return move;
    }
    
    // Check the opening book to see if it contains the current move sequence.
    // If so, it returns the next move.
    if(bookStillGood){
        int book = checkOpeningBook();
        if(book >= 0){
            move->setX(book % 8);
            move->setY(book / 8);
            if(gameboard->checkMove(move, thisside)){
                moves->push_back(book);
                gameboard->doMove(move, thisside);
                return move;
            }
        }
    }
    
    // HEURISTIC USED:
    /* For each point on the board:
        - Check if it is a valid move
        - If so, make the move
        - Create each possible response to each move.
        - Compute board value after response
            * 3 points for holding corner
            * -3 points for holding adjacent to corner
            * 1 point for every other piece
            * -1 point for each opponent piece
        - Pick and return the move with the highest minimum value after
            a response.
    */

    // These are used for the first level (calculating this player's move)
    Board *workingboard;
    Move *workingmove = new Move(-1, -1);
    int maxvalue = std::numeric_limits<int>::min();
    int workingval;
    // These are used for the 2nd level (calculating the opponent's
    // possible moves.
    Board *wboardinner;
    Move *wmoveinner = new Move(-1, -1);
    int wvalinner;
    Side otherside = BLACK;
    if(thisside == BLACK){ otherside = WHITE; }
    
    // minimax_board is created out of the moves list.
    // It is more accurate than gameboard, which sometime seems to record
    // moves incorrectly (though I haven't yet figured out why).
    Board *minimax_board = new Board();
    list<int>::iterator move_iter;
    int thismove;
    Move *tempmove = new Move(-1, -1);
    Side tempside = BLACK;
    for(move_iter = moves->begin(); move_iter != moves->end(); move_iter++){
        thismove = *move_iter;
        tempmove->setX(thismove % 8);
        tempmove->setY(thismove / 8);
        if(minimax_board->checkMove(tempmove, tempside)){
            minimax_board->doMove(tempmove, tempside);
            tempside = (tempside == BLACK) ? WHITE : BLACK;
        }
        else{
            tempside = (tempside == BLACK) ? WHITE : BLACK;
            minimax_board->doMove(tempmove, tempside);
            tempside = (tempside == BLACK) ? WHITE : BLACK;
        }
    }
    delete tempmove;

    for(int i = 0; i < 64; i++){
        workingmove->setX(i % 8);
        workingmove->setY(i / 8);
        // If this is a valid move
        if(gameboard->checkMove(workingmove, thisside)){
            // Copy the board and make the move
            workingboard = minimax_board->copy();
            workingboard->doMove(workingmove, thisside);
            /*
             * Here, we test each possible move the other side makes
             * and do a basic minimax calculation.
             * (Basically copy the code of the outer for loop here)
             */
            workingval = std::numeric_limits<int>::max();
            for(int j = 0; j < 64; j++){
                wmoveinner->setX(j % 8);
                wmoveinner->setY(j / 8);
                if(workingboard->checkMove(wmoveinner, otherside)){
                    wboardinner = workingboard->copy();
                    wboardinner->doMove(wmoveinner, otherside);
                    wvalinner = boardValue(wboardinner, thisside);
                    if(wvalinner < workingval){
                        workingval = wvalinner;
                    }
                    delete wboardinner;
                }
            }
            // If this is a better value than we already have,
            if(workingval > maxvalue){
                // Store this move as the current best.
                move->setX(workingmove->getX());
                move->setY(workingmove->getY());
                maxvalue = workingval;
            }
            // Free the memory used to copy the board.
            delete workingboard;
        }
    }
    // Free the memory used to store the moves.
    delete workingmove;
    delete wmoveinner;
    delete minimax_board;
    
    moves->push_back(move->getX() + 8*move->getY());
    gameboard->doMove(move, thisside);
    return move;
}

int ExamplePlayer::boardValue(Board *testboard, Side side){
    // valaccum is an accumulated board value
    // tempval is the value at each piece, that is added to valaccum
    int valaccum = 0, weight;
    // PART 1 : occupancy
    for(int j=0; j<64; j++){
        if(not testboard->isOccupied(j)){ break; }
        Side whichpiece = testboard->identify(j);
        // Each board position has a definite weight.
        weight = 1;
        if((j == 0) or (j == 7) or (j == 56) or (j == 63)){
            weight = 10;
        }
        if( (j == 1) or (j == 8) or (j == 9) or
            (j == 6) or (j == 14) or (j == 15) or
            (j == 48) or (j == 49) or (j == 57) or
            (j == 54) or (j == 55) or (j == 62)){
            weight = -3;
        }
        // Add the weight of a piece of the right color.
        if(whichpiece == side){
            valaccum += weight;
        }
        // Subtract 3 for a piece of the wrong color.
        else{
            valaccum -= 1;
        }
    }

    // PART 2 : Available moves (maximize)
    int available = 0;
    Move * move = new Move(-1, -1);
    for(int x=0; x<8; x++){
        for(int y=0; y<8; y++){
            move->setX(x);
            move->setY(y);
            if(testboard->checkMove(move, side)){
                available++;
            }
        }
    }
    delete move;
    return (valaccum + 1.5*available);
}


int ExamplePlayer::checkOpeningBook(){
    // Checks an opening book
    // Moves taken from http://www.samsoft.org.uk/reversi/openings.htm
    if(not bookStillGood) return -1;
    int move_return = -1;
    list<int>::iterator moves_it;
    list<vector<int> >::iterator book_it;
    vector<int>::iterator opening_it;
    // Iterate through all openings in the book.
    for(book_it = openingBook->begin();
        book_it != openingBook->end(); book_it++){
        moves_it = moves->begin();
        opening_it = book_it->begin();
        // Compare values in the current moves and in the opening.
        // If the opening is empty, break the loop.
        // If the moves are empty, return the next value in the opening.
        // If neither is empty, compare the values.
        //  - If they differ, break the loop.
        //  - If they are the same, increment both iterators.
        while(true){
            if(opening_it == book_it->end()) break;
            if(moves_it == moves->end()){
                move_return = *opening_it;
                break;
            }
            if(*moves_it != *opening_it) break;
            moves_it++;
            opening_it++;
        }
        // If we've found a move in the opening book, break the loop.
        if(move_return != -1) break;
    }
    // If there are no moves in any opening, switch bookStilGood to false.
    if(move_return == -1){
        bookStillGood = 0;
    }
    return move_return;
}

void ExamplePlayer::rotateOpeningBook(){
    list<vector<int> >::iterator outer_iter;
    vector<int>::iterator inner_iter;
    for(outer_iter = openingBook->begin();
        outer_iter != openingBook->end(); outer_iter++){
        for(inner_iter = outer_iter->begin();
            inner_iter != outer_iter->end(); inner_iter++){
            *inner_iter = 63-(*inner_iter);
        }
    }
    return;
}

void ExamplePlayer::flipOpeningBook(){
    list<vector<int> >::iterator outer_iter;
    vector<int>::iterator inner_iter;
    int p, x, y;
    for(outer_iter = openingBook->begin();
        outer_iter != openingBook->end(); outer_iter++){
        for(inner_iter = outer_iter->begin();
            inner_iter != outer_iter->end(); inner_iter++){
            p = *inner_iter;
            x = p % 8;
            y = p / 8;
            p = y + 8 * x;
            *inner_iter = p;
        }
    }
    return;
}
