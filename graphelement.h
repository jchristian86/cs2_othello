#ifndef __GRAPHELEMENT_H__
#define __GRAPHELEMENT_H__

#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <limits>
#include <list>
#include <map>
#include <vector>
#include "common.h"


using namespace std;

class GraphElement{

private:
    // Memoization that points to a graph element for different possible
    // board positions
    map<vector<bool>, GraphElement*> *extantElements;

    // The parent(s) and children of this particular board
    list<GraphElement*> parents;
    list<GraphElement*> children;
    // The position of all pieces on the board
    // 128 bits long
    //  Bit 2i is 1 if this board position is occupied, 0 if not
    //  Bit 2i+1 is 1 if this board position is black, 0 if white
    vector<bool> boardstruct;
    // Who gets the next move
    Side nextplayer;
    // What the score of this board is
    // (either calculated or bubbled up from children)
    int score;

public:
    GraphElement(vector<bool> *thisboard, Side nextplayer,
                map<vector<bool>, GraphElement*> *extantElements);
    ~GraphElement();
    
    // Basic management methods for parents/children
    void addParent(GraphElement *parent);
    void removeParent(GraphElement *parent);
    int countParents();
    void addChild(GraphElement *child);
    void removeChild(GraphElement *child);
    int countChildren();
    list<GraphElement*> *getChildren();
    
    vector<bool> *getBoard();
    
    /* A GraphElement must be able to:
        - Generate all possible children from itself given nextplayer
        (actually, I think that's about it)
        (extantElements is used for memoization purposes)
    */
    void makeChildren();
    
    void makeScore(Side side);
    int getScore();
    void setScore(int score);
    Side getSide();
    void bubbleUp(Side side);
    void resetScores(Side side);
};

#endif
